#pragma once

#include <vector>
#include <string>
#include <string_view>
//#include <charconv>

template<typename Int = int>
auto strToInt(std::string_view str, int base = 10) -> Int {
    std::make_unsigned_t<Int> val{};
    //std::from_chars(str.data(), str.data() + str.size(), val, base);
    char* endPtr;
    if constexpr(sizeof(Int) == 4) val = strtoul(str.data(), &endPtr, base);
    if constexpr(sizeof(Int) == 8) val = strtoull(str.data(), &endPtr, base);
    return (Int)val;
}

template<typename Callable>
void iter_tokens(std::string_view str, std::string_view delim, Callable func) {
    auto beg = str.find_first_not_of(delim);
    while (beg != str.npos) {
        auto wordEnd = str.find_first_of(delim, beg);
        if (func(str.substr(beg, wordEnd - beg))) return;
        beg = str.find_first_not_of(delim, wordEnd);
    }
}

template<typename String = std::string>
std::vector<String> tokenize(std::string_view str, std::string_view delim) {
    std::vector<String> tokens;
    auto beg = str.find_first_not_of(delim);
    while (beg != str.npos) {
        auto wordEnd = str.find_first_of(delim, beg);
        tokens.emplace_back(str.substr(beg, wordEnd - beg));
        beg = str.find_first_not_of(delim, wordEnd);
    }
    return tokens;
}

template<typename String = std::string>
std::vector<String> split(std::string_view str, std::string_view delim) {
    std::vector<String> tokens;
    auto beg = 0;
    auto nextSplit = str.find(delim);
    while (nextSplit != str.npos) {
        tokens.emplace_back(str.substr(beg, nextSplit));
        beg = nextSplit + delim.length();
        nextSplit = str.find(delim, beg);
    }
    if (str.size() > beg) tokens.emplace_back(str.substr(beg));
    return tokens;
}

template<typename String = std::string>
String replace(String str, std::string_view delim, std::string_view repl) {
    auto beg = 0;
    auto nextSplit = str.find(delim);
    while (nextSplit != str.npos) {
        str.replace(nextSplit, delim.length(), repl);
        beg = nextSplit + repl.length();
        nextSplit = str.find(delim, beg);
    }
    return str;
}

template<typename String = std::string>
std::pair<String, String> tokenizeFirst(std::string_view str, std::string_view delim) {
    auto split = str.find_first_of(delim);
    if (split == str.npos) return { str, "" };
    return { str.substr(0, split), str.substr(split + 1) };
}

template<typename String = std::string>
std::pair<String, String> splitFirst(std::string_view str, std::string_view delim) {
    auto split = str.find(delim);
    if (split == str.npos) return { str, "" };
    return { str.substr(0, split), str.substr(split + delim.length()) };
}

constexpr inline size_t countSubstr(std::string_view str, std::string_view sub) {
    size_t count{};
    for (auto loc = str.find(sub); loc != str.npos; ++count, loc = str.find(sub, loc + sub.length()));
    return count;
}