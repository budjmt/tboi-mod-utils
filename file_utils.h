#pragma once

#include <filesystem>
#include <fstream>
#include <cassert>

#include <iostream>

#include "string_utils.h"

namespace fs = std::filesystem;
using path_view = std::basic_string_view<fs::path::value_type>;

namespace std {
    template<> struct hash<fs::path> {
        auto operator()(const fs::path& t) const noexcept {
            return std::hash<path_view>{}(t.native());
        }
    };
}

// gets the relative path to file assuming root is already confirmed to be part of file and both are absolute paths
//template<typename Ret = fs::path>
inline fs::path simpleGetRelative(const fs::path& file, const fs::path& root) {
    //assert(root.native().length() <= file.native().length()); // either root is not actually a root of file, or at least one of the paths is not absolute
    //return file.native().c_str() + root.native().length() + (root.native().back() != '/');
    return fs::relative(file, root);
}

inline std::string file_contents(const fs::path& path) {
    std::ifstream file(path, std::ios::binary);
    if (!file.is_open()) return "";

    file.seekg(0, std::ios::end);
    auto size = (size_t) file.tellg();
    file.seekg(0, std::ios::beg);

    std::string contents(size, '\0');
    file.read(contents.data(), size);
    return contents;
}

template<typename... Args>
inline void file_write(const fs::path& path, std::ios_base::openmode flags, Args&&... strs) {
    if(path.has_parent_path() && !fs::exists(path.parent_path())) {
        std::vector<fs::path> badParents = { path.parent_path() };
        while (badParents.back().has_parent_path() && !fs::exists(badParents.back().parent_path())) badParents.push_back(badParents.back().parent_path());
        for (auto beg = rbegin(badParents); beg != rend(badParents); ++beg) fs::create_directory(*beg);
    }
    std::ofstream file(path, std::ios::binary | flags);
    const std::string_view strArr[] = { strs... };
    for (auto& str : strArr) file << str;
}