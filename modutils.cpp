#include <filesystem>
#include <iostream>

#include "include\json.hpp"

#include "file_utils.h"

namespace fs = std::filesystem;

using json = nlohmann::json;

std::vector<fs::path> getDirectories(const std::vector<std::string_view>& names, const std::vector<std::string>& directories) {
    std::vector<fs::path> outputs;
    if (names.empty()) {
        outputs.reserve(directories.size());
        for (auto& dir : directories) outputs.push_back(dir);
        return outputs;
    }

    for (auto name : names) {
        for (auto& dir : directories) {
            if (dir.find(name) == 0) {
                outputs.push_back(dir);
                break;
            }
        }
    }
    return outputs;
}

int main() {
    std::vector<std::string> directories;

    std::cout << "MOD MANAGER UTILITY\n--------------------------------------------\n";

    std::string input;
    while (true) {
        std::cout << "\n[[e]nable] or [[d]isable] mods, [save] or [load] the current state into/from a file\n";
        std::cout << "Type [[h]elp] for full command list.\n";
        std::getline(std::cin, input);
        //std::transform(begin(input), end(input), begin(input), tolower);

        directories.clear();
        for (auto dir : fs::directory_iterator(fs::current_path())) 
            if (fs::is_directory(dir)) 
                directories.push_back(simpleGetRelative(dir.path(), fs::current_path()).generic_string());

        auto [command, inputs] = tokenizeFirst<std::string_view>(input, " \t");
        if (command == "h" || command == "help") {
            std::cout << 
R"(COMMANDS: (case-insensitive)

Enable mods: e [modFolder[/modFolder2[/modFolder3...]]] OR enable [modFolder[/modFolder2[/modFolder3...]]]*
Disable mods: d [modFolder[/modFolder2[/modFolder3...]]] OR disable [modFolder[/modFolder2[/modFolder3...]]]*
*: Any mod names can be the first few letters of the folder, e.g. abpack1_848056541 can just be abpack1
   In the event of a conflict, the first directory alphabetically will be used
   If no names are provided, the command will apply to every mod in the folder

Display Enabled Mods: displayEnabled OR de
Display Disabled Mods: displayDisabled OR dd

Save current mod state: save file -> saves to file.json*
Save current mod state in compact format: compactSave file -> saves to file.json*
Load a saved mod state: load file -> loads from file.json*
Enables mods from a compact save without touching other mods: loadEnable file OR loade file OR le file
Disables mods from a compact save without touching other mods: loadDisable file OR loadd file OR ld file

Backup mod saves: backup save -> saves to save.json*
Load mod save backup: restore save -> loads from save.json*

Quit: quit OR q

*: These filenames will follow slashes for directory locations
)";
        }
        else if (command == "quit" || command == "q") {
            break;
        }
        else if (command == "displayenabled" || command == "de") {
            for (auto& dir : directories) {
                auto path = fs::path(dir) / "disable.it";
                if (!fs::exists(path)) std::cout << dir << '\n';
            }
        }
        else if (command == "displaydisabled" || command == "dd") {
            for (auto& dir : directories) {
                auto path = fs::path(dir) / "disable.it";
                if (fs::exists(path)) std::cout << dir << '\n';
            }
        }
        else if (command == "e" || command == "enable") {
            auto mods = getDirectories(tokenize<std::string_view>(inputs, "/"), directories);
            for (auto& mod : mods) {
                fs::remove(mod / "disable.it");
            }
            std::cout << "Successfully enabled mods\n";
        }
        else if (command == "d" || command == "disable") {
            auto mods = getDirectories(tokenize<std::string_view>(inputs, "/"), directories);
            for (auto& mod : mods) {
                if(fs::exists(mod)) file_write(mod / "disable.it", 0, "");
            }
            std::cout << "Successfully disabled mods\n";
        }
        else if (command == "save") {
            json output;
            output["format"] = "readable";
            for (auto& dir : directories) {
                auto path = fs::path(dir);
                output["files"][dir] = fs::exists(path) && !fs::exists(path / "disable.it");
            }
            auto path = std::string{ inputs } +".json";
            file_write(path, 0, output.dump(4));
            std::cout << "Successfully saved current state to " << path << '\n';
        }
        else if (command == "compactsave") {
            json output;
            auto& files = output["files"];
            files = json::array();
            output["format"] = "compact";
            for (auto& dir : directories) {
                auto path = fs::path(dir);
                if(fs::exists(path) && !fs::exists(path / "disable.it")) files.push_back(dir);
            }
            auto path = std::string{ inputs } +".json";
            file_write(path, 0, output.dump(4));
            std::cout << "Successfully compactly saved current state to " << path << '\n';
        }
        else if (command == "load") {
            fs::path path = std::string{ inputs } +".json";
            if (!fs::exists(path)) {
                std::cout << "The state file does not exist\n";
                continue;
            }

            auto state = json::parse(file_contents(path));
            if (!(state.count("files") && state.count("format"))) {
                std::cout << "Invalid state file\n";
                continue;
            }

            json files = state["files"];
            if (state["format"] == "compact") {
                for (auto& dir : directories) {
                    auto path = fs::path(dir);
                    if(fs::exists(path)) file_write(path / "disable.it", 0, "");
                }
                for (std::string dir : files) {
                    fs::remove(fs::path(dir) / "disable.it");
                }
            }
            else if (state["format"] == "readable") {
                for (auto& dir : directories) {
                    auto disablePath = fs::path(dir) / "disable.it";
                    if (auto iter = files.find(dir); iter != end(files)) {
                        if (*iter) fs::remove(disablePath);
                        else goto DISABLE;
                    }
                    else DISABLE: file_write(disablePath, 0, "");
                }
            }
            else {
                std::cout << "Invalid state file format\n";
                continue;
            }

            std::cout << "Successfully loaded current state from " << path << '\n';
        }
        else if (command == "loadenabled" || command == "loade" || command == "le") {
            fs::path path = std::string{ inputs } +".json";
            if (!fs::exists(path)) {
                std::cout << "The state file does not exist\n";
                continue;
            }

            auto state = json::parse(file_contents(path));
            if (!(state.count("files") && state.count("format"))) {
                std::cout << "Invalid state file\n";
                continue;
            }

            json files = state["files"];
            if (state["format"] != "compact") {
                std::cout << "Invalid state file format\n";
                continue;
            }
            for (std::string dir : files) {
                fs::remove(fs::path(dir) / "disable.it");
            }

            std::cout << "Successfully enabled from state " << path << '\n';
        }
        else if (command == "loaddisabled" || command == "loadd" || command == "ld") {
            fs::path path = std::string{ inputs } +".json";
            if (!fs::exists(path)) {
                std::cout << "The state file does not exist\n";
                continue;
            }

            auto state = json::parse(file_contents(path));
            if (!(state.count("files") && state.count("format"))) {
                std::cout << "Invalid state file\n";
                continue;
            }

            json files = state["files"];
            if (state["format"] != "compact") {
                std::cout << "Invalid state file format\n";
                continue;
            }
            for (std::string dir : files) {
                auto path = fs::path(dir);
                if (fs::exists(path)) file_write(path / "disable.it", 0, "");
            }

            std::cout << "Successfully disabled from state " << path << '\n';
        }
        else if (command == "backup") {
            json output;
            for (auto& dir : directories) {
                auto dirPath = fs::path(dir);
                for (auto& file : fs::directory_iterator(dirPath)) {
                    if (!fs::is_regular_file(file)) continue;

                    auto f = simpleGetRelative(file.path(), dirPath);
                    path_view fileView = f.native();
                    if (fileView.length() < sizeof("save.dat") - 1) continue;

                    path_view extView = { &fileView.back() - 2, 3 };
                    path_view frontView = { fileView.data(), 4 };
                    if (extView == fs::path("dat").native() && frontView == fs::path("save").native()) {
                        output["mods"][dir][fs::path(fileView.data()).generic_string()] = file_contents(file.path());
                    }
                }
            }
            auto path = std::string{ inputs } +".json";
            file_write(path, 0, output.dump(4));
            std::cout << "Successfully backed up saves to " << path << '\n';
        }
        else if (command == "restore") {
            fs::path path = std::string{ inputs } +".json";
            if (!fs::exists(path)) {
                std::cout << "The backup file does not exist\n";
                continue;
            }

            auto state = json::parse(file_contents(path));
            if (!state.count("mods")) {
                std::cout << "Invalid backup file\n";
                continue;
            }

            json mods = state["mods"];
            for (auto modIt : mods.items()) {
                auto dir = fs::path(modIt.key());
                for (auto it : modIt.value().items()) {
                    std::string contents = it.value();
                    file_write(dir / it.key(), std::ios::trunc, contents);
                }
            }

            std::cout << "Successfully restored saves from " << path << '\n';
        }
        else {
            std::cout << "Invalid Command.\n";
        }
    }
    return 0;
}